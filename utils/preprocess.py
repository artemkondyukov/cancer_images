from functools import reduce
import numpy as np
from os import walk
from os.path import join, exists
from operator import mul
from PIL import Image
import pandas as pd
from scipy.misc import imresize


def load_images(directory, image_dataset_dir, trim_threshold, size, csv_file):
    """
    Loads all images from given directory doing all related preprocessing
    :param directory: string
    :param trim_threshold: float
    :param size: int
    :return: 4d np.array (X), 1d np.array (y)
    """
    image_dataset_filename = join(image_dataset_dir, "data.npz")
    if exists(image_dataset_filename):
        np_file = np.load(image_dataset_filename)
        return np_file['x'], np_file['y']

    result_x = []
    result_y = []
    y_dict = get_filename_y_dict(csv_file)
    for root, dirs, files in walk(directory):
        for file in files:
            if "jpg" not in file:
                continue
            if file not in y_dict:
                continue
            with open('info', 'a+') as f:
                f.write("Processing file: %s\n" % file)
            image = np.array(Image.open(join(root, file)))
            image = trim_image(image, trim_threshold)
            image = resize_image(image, size)
            image = np.swapaxes(image, 0, 2) / 255.
            try:
                result_y.append(y_dict[file])
                result_x.append(image)
            except KeyError:
                continue

    np.savez_compressed(image_dataset_filename, x=result_x, y=result_y)
    return np.array(result_x), np.array(result_y)


def trim_image(image, threshold=0.8):
    """
    Crops image pixel by pixel until its mean greater than threshold
    :param image: 2d np.array
    :param threshold: float
    :return: cropped image
    """
    volume = reduce(mul, image.shape[:-1])
    min_volume = 0.7 * volume
    while volume > min_volume and image.mean() > threshold:
        image = image[5:-5, 5:-5, :]
        volume = reduce(mul, image.shape[:-1])
    return image


def resize_image(image, size):
    """
    Does resizing of an image using linear interpolation
    :param image: 2d np.array
    :param size: int
    :return: image with height and width equal to size
    """
    return imresize(image, [size, size])


def get_filename_y_dict(csv_filename):
    """
    Reads the csv given so as to produce the dict
    where keys are filenames and values are classes
    :param csv_filename: string
    :return: dict
    """
    data = pd.read_csv(csv_filename)
    return dict((x, y) for x, y in zip(data["filename"], pd.get_dummies(data["class"]).values))

