from learners.learner import Learner


class AutoencoderLearner(Learner):
    # Implements a sophisticated approach where we first
    # train CNN autoencoder and next use high-level features
    # obtained from autoencoder hidden layer so as to learn
    def fit(self, x, y):
        """
        Trains the underlying model using the data given
        :param x: 4d np.array (Yeah, it's very restrictive by now)
        :param y: float
        :return: score representing quality of learning process
        """
        pass

    def predict(self, x):
        """
        Predicts value using the underlying model
        :param x: 4d np.array
        :return: 1d np.array of floats
        """
        pass
