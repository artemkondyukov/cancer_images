import keras

from keras.layers import Dropout, Flatten, Dense
from keras.layers import Convolution2D, MaxPooling2D, UpSampling2D
from keras.models import Sequential
from learners.learner import Learner

EPOCH_NUM = 1000


class CNNLearner(Learner):
    # Implements a very basic approach to build a regression using CNN
    def __init__(self, image_size):
        self.model = Sequential()
        self.model.add(Convolution2D(8, 3, 3, activation='relu', input_shape=(3, image_size, image_size)))
        self.model.add(Convolution2D(8, 3, 3, activation='relu'))
        self.model.add(MaxPooling2D((2, 2)))

        self.model.add(Convolution2D(16, 3, 3, activation='relu'))
        self.model.add(Convolution2D(16, 3, 3, activation='relu'))
        self.model.add(MaxPooling2D((2, 2)))

        self.model.add(Convolution2D(32, 3, 3, activation='relu'))
        self.model.add(Convolution2D(32, 3, 3, activation='relu'))
        self.model.add(MaxPooling2D((2, 2)))

        self.model.add(Convolution2D(64, 3, 3, activation='relu'))
        self.model.add(Convolution2D(64, 3, 3, activation='relu'))
        self.model.add(MaxPooling2D((2, 2)))

        self.model.add(Flatten())
        self.model.add(Dense(1024, activation='sigmoid'))
        self.model.add(Dropout(0.4))

        self.model.add(Dense(5, activation='softmax'))

        self.model.compile(optimizer="adam", loss="categorical_crossentropy")

    def fit(self, x, y):
        """
        Trains the underlying model using the data given
        :param x: 4d np.array (Yeah, it's very restrictive by now)
        :param y: 2d np array of floats
        :return: score representing quality of learning process
        """
        print("X: ", x)
        print("y: ", y)
        for i in range(EPOCH_NUM):
            with open('info', 'a+') as f:
                f.write('epoch %d\n' % i)
            self.model.fit(x, y, batch_size=20, nb_epoch=1, validation_split=0.2)

    def predict(self, x):
        """
        Predicts value using the underlying model
        :param x: 4d np.array
        :return: 2d np.array of floats
        """
        return self.model.predict(x, batch_size=20)
