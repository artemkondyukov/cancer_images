from utils.preprocess import *
from learners.cnn import CNNLearner
from sys import argv

if len(argv) != 6:
    print("Usage: python main.py image_directory, image_dataset_dir," +
          "intensity_threshold_for_trim, image_size, csv_file_with_classes")
    exit(0)
X, y = load_images(argv[1], argv[2], float(argv[3]), int(argv[4]), argv[5])
learner = CNNLearner(1024)
learner.fit(X, y)
print(learner.predict(X))
